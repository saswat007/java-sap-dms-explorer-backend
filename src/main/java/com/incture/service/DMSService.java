package com.incture.service;

import java.io.IOException;
import java.io.InputStream;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.chemistry.opencmis.client.api.CmisObject;
import org.apache.chemistry.opencmis.client.api.Document;
import org.apache.chemistry.opencmis.client.api.Folder;
import org.apache.chemistry.opencmis.client.api.ItemIterable;
import org.apache.chemistry.opencmis.client.api.Session;
import org.apache.chemistry.opencmis.client.api.SessionFactory;
import org.apache.chemistry.opencmis.client.runtime.SessionFactoryImpl;
import org.apache.chemistry.opencmis.commons.SessionParameter;
import org.apache.chemistry.opencmis.commons.data.ContentStream;
import org.apache.chemistry.opencmis.commons.enums.BindingType;
import org.apache.http.Consts;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.commons.io.IOUtils;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.incture.model.DMSDocument;
import com.incture.model.DMSObject;
import com.incture.util.ApplicationConstants;
import com.incture.util.ServiceUtil;

@Service
public class DMSService {
	
	public ResponseEntity<?> getService(String FolderPath){
		
		Session session = getCmisSession(fetchDMSToken());
		
		List<DMSObject> responseList = new ArrayList<>();
		
		String[] path = FolderPath.split("/"); 
		
		if(path.length==1) {
			Folder root = session.getRootFolder();
			ItemIterable<CmisObject> crewFolderList = root.getChildren();
			
			for (CmisObject crewFolder : crewFolderList) {
				responseList.add(new DMSObject(crewFolder.getName(),crewFolder.getId()));
			}
			return new ResponseEntity<List>(responseList,HttpStatus.OK);		
		}

		else if(path.length==2) {
			Folder root = session.getRootFolder();
			ItemIterable<CmisObject> crewFolderList = root.getChildren();
			ItemIterable<CmisObject> documentFolderList = null;
			
			for (CmisObject crewFolder : crewFolderList) {
				if (crewFolder.getName().equalsIgnoreCase(path[1])) {
					Folder cf = (Folder)crewFolder;
					documentFolderList = cf.getChildren();
				}
			}
			
			for (CmisObject documentFolder : documentFolderList) {
				responseList.add(new DMSObject(documentFolder.getName(),documentFolder.getId()));
			}
			return new ResponseEntity<List>(responseList,HttpStatus.OK);		
		}
		
		else if(path.length==3) {
			Folder root = session.getRootFolder();
			ItemIterable<CmisObject> crewFolderList = root.getChildren();
			ItemIterable<CmisObject> documentFolderList = null;
			ItemIterable<CmisObject> documentList = null;
			
			for (CmisObject crewFolder : crewFolderList) {
				if (crewFolder.getName().equalsIgnoreCase(path[1])) {
					Folder cf = (Folder)crewFolder;
					documentFolderList = cf.getChildren();
				}
			}
			
			for (CmisObject documentFolder : documentFolderList) {
				if (documentFolder.getName().equalsIgnoreCase(path[2])) {
					Folder df = (Folder)documentFolder;
					documentList = df.getChildren();
				}
			}
			
			for (CmisObject document : documentList) {
				//responseList.add(new DMSObject(document.getName(),document.getId()));
				responseList.add(new DMSObject(document.getName(),getOldestId(document)));
			}
			return new ResponseEntity<List>(responseList,HttpStatus.OK);		
		}
		return new ResponseEntity<String>("",HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	public String getOldestId(CmisObject documentObject) {
		Document document = (Document) documentObject;
		List<Document> documentVersionList = document.getAllVersions();
//		System.out.println(documentVersionList.get(0).getId());
//		System.out.println(documentVersionList.get(1).getId());
		return documentVersionList.get(0).getId();
	}
	
//	public ResponseEntity<?>download(String ObjectId){
//		Session session = getCmisSession(fetchDMSToken());
//		Document document = (Document) session.getObject(ObjectId);
//		InputStream content = document.getContentStream().getStream();
//		try {
//			byte[] data = content.readAllBytes();
//			return new ResponseEntity<byte[]>(data,HttpStatus.OK);
//		} catch (IOException e) {
//			e.printStackTrace();
//			return new ResponseEntity<String>("",HttpStatus.INTERNAL_SERVER_ERROR);
//		}
//	}
	
	public ResponseEntity<?>download2(String ObjectId){
		Session session = getCmisSession(fetchDMSToken());
		Document document = (Document) session.getObject(ObjectId);
		try {
			InputStream content = document.getContentStream().getStream();
			byte[] bytes = IOUtils.toByteArray(content);
			String encoded = Base64.getEncoder().encodeToString(bytes);
			return new ResponseEntity<DMSDocument>(new DMSDocument(encoded),HttpStatus.OK);
		} catch (IOException e) {
			e.printStackTrace();
			return new ResponseEntity<String>("",HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
//	public ResponseEntity<?>download3(String ObjectId){
//		Session session = getCmisSession(fetchDMSToken());
//		Document document = (Document) session.getObject(ObjectId);
//		try {
//			ContentStream content = document.getContentStream();
//			String data = ServiceUtil.getContentAsString(content);
//			String encodedString = Base64.getEncoder().encodeToString(data.getBytes());
//			return new ResponseEntity<String>(data,HttpStatus.OK);
//		} catch (IOException e) {
//			e.printStackTrace();
//			return new ResponseEntity<String>("",HttpStatus.INTERNAL_SERVER_ERROR);
//		}
//	}
	
	public ResponseEntity<?>information(String ObjectId){
		Session session = getCmisSession(fetchDMSToken());
		Document document = (Document) session.getObject(ObjectId);
		long epoch = document.getCreationDate().getTimeInMillis()/1000;
		System.out.println(epoch);
		Instant instant = Instant.ofEpochSecond(epoch);
	    LocalDate localDate = instant.atZone(ZoneId.systemDefault()).toLocalDate();
		DMSDocument documentInfo = new DMSDocument(document.getContentStreamMimeType(),document.getContentStreamLength(),localDate);
		return new ResponseEntity<DMSDocument>(documentInfo,HttpStatus.OK);
	}

	private Session getCmisSession(String token) {
		SessionFactory factory = SessionFactoryImpl.newInstance();
		Map<String, String> parameter = new HashMap<String, String>();

		// connection settings
		parameter.put(SessionParameter.BINDING_TYPE, BindingType.BROWSER.value());
		parameter.put(SessionParameter.BROWSER_URL, "https://api-sdm-di.cfapps.us10.hana.ondemand.com/browser");
		parameter.put(SessionParameter.OAUTH_ACCESS_TOKEN, token);
		parameter.put(SessionParameter.AUTH_HTTP_BASIC, "false");
		parameter.put(SessionParameter.AUTH_SOAP_USERNAMETOKEN, "false");
		parameter.put(SessionParameter.AUTH_OAUTH_BEARER, "true");
		parameter.put(SessionParameter.USER_AGENT,
				"OpenCMIS-Workbench/1.1.0 Apache-Chemistry-OpenCMIS/1.1.0 (Java 1.8.0_271; Windows 10 10.0)");
		
		return factory.getRepositories(parameter).get(0).createSession();
	}
	
	
	private static String fetchDMSToken() {
		 
		try (CloseableHttpClient client = HttpClients.createDefault()) {
		      HttpPost httpPost = new HttpPost(ApplicationConstants.DMS_TOKEN_ENDPOINT);

		      httpPost.addHeader("Content-Type", "application/x-www-form-urlencoded");

		      List<NameValuePair> form = new ArrayList<>();
		      form.add(new BasicNameValuePair("grant_type", ApplicationConstants.DMS_GRANT_TYPE));
		      form.add(new BasicNameValuePair("client_id", ApplicationConstants.DMS_CLIENTID));
		      form.add(new BasicNameValuePair("client_secret", ApplicationConstants.DMS_CLIENTSECRET));
		      UrlEncodedFormEntity entity = new UrlEncodedFormEntity(form, Consts.UTF_8);

		      httpPost.setEntity(entity);

		      CloseableHttpResponse response = client.execute(httpPost);

		      String responseApiCall = ServiceUtil.getDataFromStream(response.getEntity().getContent());
		      JSONObject jsonObj = new JSONObject(responseApiCall);
		      return jsonObj.getString("access_token");
		      
		    } catch (Exception e) {
		      e.printStackTrace();
		      return "Error:" + e.getMessage();
		    }
		  }

}
