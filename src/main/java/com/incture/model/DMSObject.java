package com.incture.model;

public class DMSObject{
	
	String name;
	String objectId;
	
	
	
	public DMSObject(String name, String objectId) {
		super();
		this.name = name;
		this.objectId = objectId;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getObjectId() {
		return objectId;
	}
	public void setObjectId(String objectId) {
		this.objectId = objectId;
	}
}
