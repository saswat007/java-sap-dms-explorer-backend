package com.incture.model;

import java.time.LocalDate;

public class DMSDocument {
	String base64;
	String fileType;
	long size;
	LocalDate createdOn;

	public DMSDocument(String fileType, long size, LocalDate createdOn) {
		super();
		this.fileType = fileType;
		this.size = size;
		this.createdOn = createdOn;
	}
	
	public DMSDocument(String base64) {
		this.base64 = base64;
	}
	
	public String getBase64() {
		return base64;
	}

	public void setBase64(String base64) {
		this.base64 = base64;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public long getSize() {
		return size;
	}

	public void setSize(long size) {
		this.size = size;
	}

	public LocalDate getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(LocalDate createdOn) {
		this.createdOn = createdOn;
	}
	
	

}
