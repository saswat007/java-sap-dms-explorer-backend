package com.incture.controller;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.incture.service.DMSService;

@RestController
@RequestMapping("/api/v1")
@CrossOrigin
public class DMSController {
	
	@Autowired
	DMSService service;
	
	@PostMapping("/folders")
	public ResponseEntity<?> get(@RequestBody String payload){
		JSONObject json = new JSONObject(payload);
		String FolderPath = json.getString("folderPath");
		
		return service.getService(FolderPath);
	}
	
	@PostMapping("/folders/download")
	public ResponseEntity<?> download(@RequestBody String payload){
		JSONObject json = new JSONObject(payload);
		String ObjectId = json.getString("objectId");
		
		return service.download2(ObjectId);
	}
	
	@PostMapping("/folders/information")
	public ResponseEntity<?> information(@RequestBody String payload){
		JSONObject json = new JSONObject(payload);
		String ObjectId = json.getString("objectId");
		
		return service.information(ObjectId);
	}

}
